# -*- coding: utf-8 -*-

# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# January 2021

# Import libraries and data
from sklearn.datasets import load_breast_cancer
data = load_breast_cancer()

print(data.target[[10,50,85]]) # Class' list

print(data.feature_names) # Attribute's names

list(data.target_names)

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(data.data, data.target, test_size=0.2)
print(X_train.shape, y_train.shape)
print(X_test.shape, y_test.shape)

"""**Let's create and train a model**"""

from sklearn.tree import DecisionTreeClassifier
dt = DecisionTreeClassifier(random_state=0) # Departure point

"""**Fit the model on the training data**"""

model = dt.fit(X_train, y_train)

"""**Let's use our model for prediction on the test set**"""

prediction = dt.predict(X_test) # Prediction on the 114 instance's test
print(prediction)

"""**Let's checkout the performance of our model (accuracy)**"""

print("Score:", model.score(X_test, y_test))

"""**Try other metrics**

"""

from sklearn import metrics
print("Classification Accuracy :\n", metrics.accuracy_score(y_test, prediction))
print("Classification F1-score :\n", metrics.f1_score(y_test, prediction))

#or
print("Classification report:\n", metrics.classification_report(y_test, prediction))

"""**Confusion matrix**"""

print("Confusion matrix\n", metrics.confusion_matrix(y_test, prediction))

"""**Plot the confusion matrix**"""

# Commented out IPython magic to ensure Python compatibility.
import matplotlib.pyplot as plt
# %matplotlib inline

titles_options=[("Confusion matrix (without normalisation", None),
                ("Confusion matrix (normalized)", 'true')]
for title, normalize in titles_options:
  disp= metrics.plot_confusion_matrix(model, X_test, y_test,
                                      display_labels=data.target_names,
                                      cmap=plt.cm.Blues,
                                      normalize=normalize)
  disp.ax_.set_title("Confusion matrix, without normalization")
  print(title)
  print(disp.confusion_matrix)
  plt.show()

"""## **K-Folds Cross Validation**
-We use k-1 subsets for train our model and leave the last subset for testing

-We repeat the evaluation k times and average the performance results
"""

from sklearn.model_selection import KFold 
kf = KFold(n_splits=5)
kf.get_n_splits()

print(kf)

"""**Let's split our dataset**

**Question :** Add the code to compute the model performance using this k-fold CV
"""

for train_index, test_index in kf.split(data.data) :
  X_train, X_test = data.data[train_index], data.data[test_index]
  y_train, y_test = data.target[train_index], data.target[test_index]

"""**Let's create and train a model**"""

from sklearn.tree import DecisionTreeClassifier 
from sklearn import metrics

dt = DecisionTreeClassifier()
accuracies = []

for train_index, test_index in kf.split(data.data) :
  # print ("TRAIN:", train_index, "TEST:", test_index)
  X_train, X_test = data.data[train_index], data.data[test_index]
  y_train, y_test = data.target[train_index], data.target[test_index]

  model = dt.fit(X_train, y_train)
  prediction = model.predict(X_test)

  score = metrics.accuracy_score(y_test, prediction)
  accuracies.append(score)

  print(accuracies)
  print(sum(accuracies)/len(accuracies))

"""Leave one out validation LOO or LOOCV

LOO is another CV technique, except that in this type of cross validation, the number of folds (subsets) equals to the number of observations in the dataset
"""

from sklearn.model_selection import LeaveOneOut
loo = LeaveOneOut()
loo.get_n_splits(data.data)

for train_index, test_index in loo.split(data.data) :
  X_train, X_test = data.data[train_index], data.data[test_index]
  y_train, y_test = data.target[train_index], data.target[test_index]

"""Question : Add the code to compute the model performance using this LOO CV"""

from sklearn import metrics

from sklearn.tree import DecisionTreeClassifier
dt = DecisionTreeClassifier(random_state=0)
accuracies = []

for train_index, test_index in loo.split(data.data) :
  X_train, X_test = data.data[train_index], data.data[test_index]
  y_train, y_test = data.target[train_index], data.target[test_index]
  dt.fit(X_train, y_train)
  prediction = dt.predict(X_test)
  accuracies.append(metrics.accuracy_score(y_test, prediction))

  print(accuracies)
  print("Average accuracy is:" + str(sum(accuracies)/len(accuracies)))

"""Another way to perform cross validation is : """

from sklearn.model_selection import cross_val_score
from sklearn import metrics
scores = cross_val_score(dt, data.data, data.target, cv=5)
print("Cross-validated scores:", scores)
print("Cross-validated mean scores", sum(scores)/len(scores))
