***Introduction to Machine Learning, Evaluation metrics and methods***

**Step 1**

Let's create and train a model.

**Step 2**

Fit the model on the training data.

**Step 3**

Let's use our model for prediction on the test set.

**Step 4**

Let's checkout the performance of our model (accuracy and other metrics).

**Step 5**

Confusion matrix.

**Step 6**

K-Folds Cross Validation.

We use k-1 subsets for train our model and leave the last subset for testing.

We repeat the evaluation k times and average the performance results.



Author : Marion Estoup

E-mail : marion_110@hotmail.fr

January 2021
